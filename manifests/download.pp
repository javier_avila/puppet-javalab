# == Class: javalab::download
#
#   Download the jdk file

class javalab::download(
    $file_name              = $javalab::file_name,
    $dir_target             = $javalab::destination_path,
    $ora_url                = $javalab::ora_url,
    $cookie                 = $javalab::params::cookie,
    $puppet_url             = $javalab::base_source_path,
    $download_from_master   = $javalab::master_source,
    $java_version           = $javalab::params_java_version
) {
    if $javalab::master_source {
        file { "${dir_target}${file_name}":
            source  => "${puppet_url}${file_name}"
        }
    }
    else {
        case $::operatingsystem {
            'windows' : {
                pget {'Download requires Cookie':
                    source      => "${ora_url}",
                    target      => "${dir_target}",
                    headerHash  => {
                        'Cookie' => "${cookie}"
                    }
                }
            }
            default : {
                exec { "download ${dir_target}${file_name}":
                    command => "curl -L '${ora_url}' -H 'Cookie:${cookie}' -o ${destination_path}${file_name}",
                    cwd => "${dir_target}",
                    alias => "download-jdk",
                    path    => ["/bin", "/usr/bin", "/usr/sbin"],
                }
            }
        }
    }
}