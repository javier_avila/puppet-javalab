# == Class: javalab::install
#
# Install the jdk package
#
class javalab::install(
    $dir_target         = $javalab::destination_path,
    $file_name          = $javalab::file_name,
    $file_name_base     = $javalab::file_name_base,
    $file_format        = $javalab::params::file_format,
    $instalation_dir    = $javalab::params::instalation_dir
) {
    $java_home          = "$instalation_dir/$file_name_base"
    case $::operatingsystem {
        'lion', 'mountain lion', 'mavericks', 'Darwin', 'yosemite', 'el capitan': {
            package { 'install jdk':
                source      => "${dir_target}${file_name}",
                ensure      => installed
            }

        }
        'windows' : {
            package { 'install jdk':
                source      => "${dir_target}${file_name}",
                ensure      => installed,
                install_options => ['/passive /quiet']
            }
        }
        'ubuntu' : {
            archive::extract {"${file_name_base}":
                src_target          => "/tmp",
                target              => "${java_home}",
                extension           => "${file_format}",
                strip_components    => 1,
                ensure              => present,
                before              => Exec['Update alternatives for java']
            }
            include javalab::environment
            include javalab::alternatives
            Class['javalab::environment'] -> Class['javalab::alternatives']
        }
    }
}