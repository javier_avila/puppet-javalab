# == Class: javalab::params
#
#
# Default parameters
class javalab::params {
    $version                    = '7'
    $min_version                = 'u79'
    $build                      = 'b15'
    $master_source              = false
    $oracle_url                 = 'http://download.oracle.com/otn-pub/java/jdk/'
    $base_source_path           = 'puppet:///modules/javalab/'
    $cookie                     = 'oraclelicense=accept-securebackup-cookie'
    $priority                   = 10000

    case $::architecture {
        /x86_64|amd64|x64/  :   { $arch = 'x64' }
        'x86'               :   { $arch = 'i586' }
        default             :   { fail("My module doesn't support that architecture ${::architecture}") }
    }

    case $::operatingsystem {
        'lion', 'mountain lion', 'mavericks', 'Darwin', 'yosemite', 'el capitan': {
            $destination_path   = '/tmp/'
            $instalation_user   = 'root'
            $instalation_group  = 'root'
            $file_format        = 'dmg'
            $so_ref             = 'macosx'
        }
        'ubuntu': {
            $destination_path   = '/tmp/'
            $instalation_dir    = '/usr/local'
            $instalation_user   = 'root'
            $instalation_group  = 'root'
            $file_format        = 'tar.gz'
            $so_ref             = 'linux'
        }
        'windows' : {
            $destination_path   = 'C:\\Program Files\\'
            $instalation_user   = 'Administrator'
            $instalation_group  = 'Administrator'
            $file_format        = 'exe'
            $so_ref             = 'windows'
        }
        default: {
            fail("My module does not support that operative system: ${::operatingsystem}")
        }
    }
}
