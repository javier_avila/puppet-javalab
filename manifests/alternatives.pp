# == Class: javalab::alternatives
#
#   Set alternatives (Only for ubuntu nodes)
#
# 
class javalab::alternatives(
    $file_name_base     = $javalab::file_name_base,
    $instalation_dir    = $javalab::instalation_dir
) {
    $java_home  = "$instalation_dir/$file_name_base"
    exec {"Update alternatives for java":
        provider    => shell,
        command     => "for i in \$(ls ${java_home}/bin/) ; do update-alternatives --install /usr/bin/\$i \$i ${java_home}/bin/\$i 10000; done;"
    }
}