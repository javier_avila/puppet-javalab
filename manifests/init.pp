#   == Class: javalab::init
#
#
#   This module installs the jdk package


class javalab (
    $version           = $javalab::params::version,
    $min_version       = $javalab::params::min_version,
    $build             = $javalab::params::build,
    $path              = $javalab::params::path,
    $master_source     = $javalab::params::master_source,
    $destination_path  = $javalab::params::destination_path,
    $instalation_user  = $javalab::params::instalation_user,
    $instalation_group = $javalab::params::instalation_group
) inherits javalab::params {
    validate_re($version, '^7|8')
    if $master_source {
        validate_string($build)
    }
    $file_name  = "jdk-${version}${min_version}-${javalab::params::so_ref}-${javalab::params::arch}.${javalab::params::file_format}"

    $file_name_base  = "jdk-${version}${min_version}-${javalab::params::so_ref}-${javalab::params::arch}"
    
    $ora_url = "$javalab::params::oracle_url$javalab::version$javalab::min_version-$javalab::build/$file_name"
    # TODO Check all params
    include javalab::download
    include javalab::install
    Class['javalab::download'] -> Class['javalab::install']
}
