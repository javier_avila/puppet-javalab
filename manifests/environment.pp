# == Class: javalab::env
#
#   Set java_home in ubuntu
#

class javalab::environment{
  augeas { 'environment':
    context => '/files/etc/environment',
    changes => [ "set JAVA_HOME ${javalab::java_home}/bin/java" ]
  }
}

