# javalab

#### Table of Contents

1. [Overview](#overview)
2. [Module Description](#Module Description)
3. [Setup - The basics of getting started with javalab](#setup)
4. [Usage - Configuration options and additional functionality](#usage)
5. [Limitations - OS compatibility, etc.](#limitations)

## Overview

This module is a multiplatform oracle-jdk intaller, it can install from the
master puppet node or from the oracle webpage .

## Module Description

It can install the packages from the master puppet node or from the oracle webpage .


## Setup

Before start ensure the cyberious-pget and puppetlabs-stdlib are installed in your
puppet master node.

If you want to use your puppet master node to distribute the packages, you have to
download the require packages into files folder.

### Beginning with javalab

The default usage is

```
class { 'javalab'}
```




## Usage

The default javalab class sets up the default package to jdk7u19-b15

You can customize the jdk version that you want to install. For instance this delcaration install the
package jdk-8u65 from the master node.


```
class { 'javalab':
    version => '8',
    min_version => 'u65',
}
```


You can customize the package source between master puppet to oracle web page.
this declaration installs Java from the master puppet.


```
class { 'javalab':
    master_source => true,
}
```


Note: In order to download a diferent version from the oracle web page, you also have to customize the 
build. For example oracle_url/jdk/8u65-b17/jdk-8u65-windows-x64.exe, the b17 is the build


## Limitations

You shoud not use this module in a production environment (yet).
It was tested in ubuntu14:04, windows 10, windows 7, mac os (darwin)